package com.useopen.as;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class TransferSimple2Traditonal {

    public static int countProperties = 0;
    public static HashMap<String, String> ItTerms = new HashMap<>();

    /*
    * 1.遍历所有目录下文件名称中包含 zh_Hans.properties 字符串的文本
    * 2.将该文本中的简体中文编码转换为繁体中文编码
    * 3.在该目录下生成 *zh_Hant.propreties 的文本
    */
    public static void main(String[] args) {

        readExcel(new File("/Users/xqd/Desktop/terms.xlsx"));
//        getFileList("/Users/xqd/Downloads/uoas2");
        getFileList("/Users/xqd/Downloads/uoas2");


    }

    public static void getFileList(String path) {

        /*主文件夹*/
        File rootDir = new File(path);

        /*主文件夹下所有的子文件夹和文件*/
        File[] subDir = rootDir.listFiles();

        for (int i = 0; i < subDir.length; i++) {

            /* if the subDir[i] is a directory , then to traversal the directory recursively*/
            if (subDir[i].isDirectory()) {
                String subPath = subDir[i].getAbsolutePath();
                getFileList(subPath);
            } else {
                String absolutePath = subDir[i].getAbsolutePath();
                String fileName = subDir[i].getName();

                /*判断文件是否为LocalDescription_zh_Hans.properties*/
                boolean isHansProperties = getExtensionName(fileName);
                boolean isProperties = getExtentionNameProperties(absolutePath);
                if (isHansProperties) {
                    /*开启一个子线程,传递文件的绝对路径到*/
                    new Thread(new PropetyThread(absolutePath)).start();
                }

                if (isProperties) {
                    System.out.println(absolutePath);
                    countProperties++;
                    System.out.println(countProperties + "");
                }
            }
        }
    }


    /*判断是否包含zh_Hans.properties扩展名"*/
    public static boolean getExtensionName(String filename) {

        if ((filename != null) && (filename.length() > 0)) {
            boolean isContains = filename.contains("zh_Hans.properties");
            boolean isHasTarget = filename.contains("target");

            if (isContains && !isHasTarget) {
                return true;
            }
        }
        return false;
    }


    /*统计还有多少个尚未翻译的英文文件*/
    public static boolean getExtentionNameProperties(String fileName) {

        boolean isProperties = fileName.contains(".properties");

        /*是否为一个.properties文件*/
        if ((fileName != null) && (fileName.length() > 0) && isProperties) {

            /*是否为一个简体中文文件*/
            boolean isHans = fileName.contains("_zh_Hans.properties");
            boolean isHant = fileName.contains("_zh_Hant.properties");

            int index = fileName.indexOf(".properties");
            String hansPath = fileName.substring(0, index) + "_zh_Hans.properties";
            File file = new File(hansPath);

            boolean isHasTarget = fileName.contains("target");

            /*如果是.properties文件,且当前目录下不包含_zh_Hans.properties文件,且路径中不包含target字符串*/
            if (!isHans && !isHant && isProperties && !file.exists() && !isHasTarget) {
                return true;
            }
        }
        return false;
    }


    private static void readExcel(File file) {
        List<List<Object>> list = new LinkedList<List<Object>>();

        // 构造 XSSFWorkbook 对象，strPath 传入文件路径
        XSSFWorkbook xwb = null;

        try {
            xwb = new XSSFWorkbook(new FileInputStream(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 读取第一章表格内容,一个excel文件中有多个表格
        XSSFSheet sheet = xwb.getSheetAt(0);

        XSSFRow row = null;


        for (int i = 2; i < sheet.getPhysicalNumberOfRows(); i++) {

            /*获取当前行*/
            row = sheet.getRow(i);

            if (row == null) {
                continue;
            }

            /*得到key*/
            XSSFCell keyCell = row.getCell(2);
            /*得到value*/
            XSSFCell valueCell = row.getCell(1);

            String key = keyCell.getRichStringCellValue().getString();
            String value = valueCell.getRichStringCellValue().getString();

            String returnValue = ItTerms.put(key, value);
//            if (!returnValue.equals("") && !"".equals(returnValue)) {
//                continue;
//            }
        }

    }


}
