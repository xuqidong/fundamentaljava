package com.useopen.as;

import com.luhuiguo.chinese.ChineseUtils;

import java.io.*;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by xqd on 17/05/2017.
 */
public class PropetyThread implements Runnable {

    /*Hans文件的路径*/
    private String copyPath = null;
    /*Hant文件的路径*/
    private String newPath = null;


    public PropetyThread(String absolutePath) {
        copyPath = absolutePath;
        newPath = absolutePath.replace("/LocalDescriptions_zh_Hans.properties", "/LocalDescriptions_zh_Hant.properties");

        /*新文件路径*/
        System.out.println("新文件路径" + newPath);
    }

    @Override
    public void run() {
        try {

            /*Hans文件*/
            File copyFile = new File(copyPath);
            InputStreamReader reader = new InputStreamReader(new FileInputStream(copyFile));
            BufferedReader br = new BufferedReader(reader);

            /* Hant文件 */
            File newFile = new File(newPath);

            /*如果要生成的文件已经存在则删除进行重新生成*/
            if (newFile.exists()) {
                newFile.delete();
//                newFile.deleteOnExit();
            }

            newFile.createNewFile();

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(newFile));
            BufferedWriter out = new BufferedWriter(outputStreamWriter);

            String line = null;

            while ((line = br.readLine()) != null) {
                if (line.contains("=")) {
                    int index = line.indexOf("=");
                    /*截取 "=" 以后的内容*/
                    String ascString = line.substring(index + 1, line.length());
                    /*ASCII 转简体中文*/
                    String nativeString = Native2ASC.ascii2Native(ascString);

                    /*在这一步进行查找,如果map中存在替换的IT术语,就进行替换,然后再将简体翻译为繁体*/
                    nativeString = checkTerms(nativeString);

                    /*简体中文转繁体中文*/
                    String traditional = ChineseUtils.toTraditional(nativeString);
                    /*繁体中文转 ASCII*/
                    String ascii = Native2ASC.native2Ascii(traditional);

                    String prefixName = line.substring(0, index + 1);
                    /*繁体中文编码*/
                    line = prefixName + ascii;

                    out.write(line, 0, line.length());
                    out.newLine();
                } else {
                    out.write(line, 0, line.length());
                    out.newLine();
                }
            }
            out.flush();
            out.close(); // 最后记得关闭文件
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String checkTerms(String nativeString) {
        String newNativeString = null;

        Set<String> keySet = TransferSimple2Traditonal.ItTerms.keySet();
        Iterator<String> iterator = keySet.iterator();
        while (iterator.hasNext()) {
            String key = iterator.next();

            if (nativeString.contains(key)) {
                String value = TransferSimple2Traditonal.ItTerms.get(key);

                /*使用港台惯用术语代替大陆惯用术语*/
                newNativeString = nativeString.replace(key, value);

                return  newNativeString;
            }
        }
        return nativeString;
    }

}
