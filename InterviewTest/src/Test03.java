import java.util.concurrent.CountDownLatch;

/**
 * Created by xqd on 2017/4/22.
 */
public class Test03 {

    public static void main(String[] args) throws InterruptedException {
        long time = timeTask(10, new MyThread());
        System.out.println("10个线程的时间总计为:" + time + "毫秒");
    }

    public static long timeTask(int countThreads, final Runnable task) {
        final CountDownLatch startGate = new CountDownLatch(1);
        final CountDownLatch endGate = new CountDownLatch(countThreads);

        for (int i = 0; i < countThreads; i++) {
            Thread thread = new Thread() {
                public void run() {
                    try {
                        startGate.await();
                        task.run();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } finally {
                        endGate.countDown();
                    }
                }
            };
            thread.start();
        }
        long start = System.nanoTime();
        startGate.countDown();
        endGate.countDown();
        long end = System.nanoTime();
        return end - start;
    }

    static class MyThread implements Runnable {

        volatile int count = 0;

        @Override
        public void run() {
            count++;
            System.out.println(count);
        }
    }
}
