import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Created by xqd on 2017/4/22.
 */
public class Test02 {

    public static void main(String[] args) {
//        20, 4, 6, 50, 82, 100, 11, 5, 45, 60
        int[] arr = {10, 9, 10, 44, 33, 20, 4, 6, 50, 82, 100, 11, 5, 45, 60};

        findByK(5, arr);
    }

    public static void findByK(int key, int[] arrays) {
        int index = 0;
        /*记录当前的值*/
        int currentArrays = 0;
        /*记录当前的最小位置*/
        String position = null;
        Map<String, Integer> map = new TreeMap<>();

        for (int i = 0; i < arrays.length; i++) {
            if (i < key) {
                index++;
                map.put(index + "", arrays[i]);
            } else {
                Set<String> keySet = map.keySet();
                currentArrays = arrays[i];
                for (String set : keySet) {
                    Integer integer = map.get(set);
                    if (integer < currentArrays) {
                        /*记录当前最小的值*/
                        currentArrays = integer;
                        /*记录最小值的key*/
                        position = set;
                    }
                }
                if (position != null)
                    map.put(position, arrays[i]);
                position = null;
            }
        }

        Set<String> keySet = map.keySet();
        for (String set : keySet) {
            Integer integer = map.get(set);
            System.out.println(integer);
        }
    }
}
