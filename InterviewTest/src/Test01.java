import java.util.ArrayList;

/**
 * Created by xqd on 2017/4/22.
 */
public class Test01 {

    public static void main(String[] args){
        ArrayList<String> list = new ArrayList<>();
        list.add("java");
        list.add("JavaScript");
        list.add("java");
        list.add("zookeeper");
        list.add("dubbo");

        for(int i  = 0;i < list.size();i++){
            if("java".equals(list.get(i))){
                list.remove(i);
            }
        }

        for (int i = 0 ;i < list.size();i++){
            System.out.println(list.get(i));
        }
    }
}
