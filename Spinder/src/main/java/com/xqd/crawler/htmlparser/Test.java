package com.xqd.crawler.htmlparser;


import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.util.NodeList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by xqd on 2017/4/12.
 */
public class Test {

    /*存放未访问的URL*/
    private static UnVisitedQueue unVisitedQueue = new UnVisitedQueue();
    /*存放已经访问的URL*/
    private static VisitedQueue visitedQueue = new VisitedQueue();

    public static void main(String[] args) throws InterruptedException {
        /*提取URL的线程池*/
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        /*提取网页中的内容的线程池*/
        ExecutorService contentExecutor = Executors.newSingleThreadExecutor();
        /*添加种子URL到待访问队列中*/
        unVisitedQueue.addUrl("https://book.douban.com/tag/历史?start=20&amp;type=T");
        unVisitedQueue.addUrl("https://book.douban.com/tag/%E5%8E%86%E5%8F%B2");
        /*获取更多的URL*/
        while (!unVisitedQueue.isEmpty()) {

            if (!unVisitedQueue.isEmpty()) {
                Thread.sleep(2000);
            }
            /*队头出列*/
            String url = unVisitedQueue.getUrl();

            /*当前正在访问的URL*/
            System.out.println(url);

            /*加入到已访问队列中*/
            visitedQueue.addVisitedUrl(url);
            /*提取网页中的相关URL*/
            executorService.execute(new ExtraUrl(url));
            /*提取网页中的数据*/
            contentExecutor.execute(new ParserHtml(url));
//            executorService.execute(new ParserHtml(url));
        }

    }


    static class ExtraUrl extends Thread {
        private String originalUrl = "https://book.douban.com";
        private String visitingUrl = null;
        private volatile static int count = 0;

        public ExtraUrl(String url) {
            System.out.println("当前线程为:" + Thread.currentThread().getName());
            visitingUrl = url;
        }

        @Override
        public void run() {
            try {

                CloseableHttpClient client = HttpClients.createDefault();
                HttpGet get = new HttpGet(visitingUrl);

            /*设置header*/

                get.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");

                get.setHeader("Accept-Charset", "GB2312,utf-8;q=0.7,*;q=0.7");

                get.setHeader("Accept-Encoding", "gzip, deflate, sdch, br");

                get.setHeader("Accept-Language", "zh-CN,zh;q=0.8,en;q=0.6");

                get.setHeader("Connection", "keep-alive");

                get.setHeader("Cookie", "bid=zXRhJtzS0p8; gr_user_id=a23abf37-85d0-44c0-b51e-4a656223b1af; ll=\"108288\"; viewed=\"25985021_26704403_26698660_2580604\"; _vwo_uuid_v2=5A5AE161E675BA40D5EC4477780639F2|76444d2b9b963e5f5d831d123d96ea7d; _pk_ref.100001.3ac3=%5B%22%22%2C%22%22%2C1492231575%2C%22https%3A%2F%2Fwww.douban.com%2F%22%5D; __utmt=1; __utmt_douban=1; __utma=30149280.208063152.1490357727.1492221403.1492231514.17; __utmb=30149280.5.10.1492231514; __utmc=30149280; __utmz=30149280.1492221403.16.10.utmcsr=douban.com|utmccn=(referral)|utmcmd=referral|utmcct=/; __utma=81379588.1170892467.1490708551.1492221403.1492231575.11; __utmb=81379588.2.10.1492231575; __utmc=81379588; __utmz=81379588.1492231575.11.6.utmcsr=douban.com|utmccn=(referral)|utmcmd=referral|utmcct=/; _pk_id.100001.3ac3=bdadb79dbd3042c8.1490708551.11.1492232736.1492221402.; _pk_ses.100001.3ac3=*; ap=1\n" +
                        "Host:book.douban.com");

                get.setHeader("refer", "https://www.douban.com/");

                get.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36");

            /*响应*/
                CloseableHttpResponse response = client.execute(get);

            /*存放HTML文本*/
                String html = null;

                if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                    HttpEntity entity = response.getEntity();
                    InputStream content = entity.getContent();
                    html = parseEntity(content);
                }

            /*也可以接收String类型的数据啊*/
                Parser parser = new Parser(html);

                NodeFilter attributeFilter = new HasAttributeFilter("href");
                NodeList nodes = parser.extractAllNodesThatMatch(attributeFilter);

                if (nodes != null) {
                    for (int i = 0; i < nodes.size(); i++) {
                        Node textnode = (Node) nodes.elementAt(i);
                        String text = textnode.getText();
                    /*找出起始位置*/
                        int startPosition = text.indexOf("/tag");

                        if (startPosition != -1) {
                            String targetUrl = text.substring(startPosition, text.length() - 2);

                            String realUrl = null;
                            if (regexString(targetUrl)) {
                                realUrl = originalUrl + targetUrl;
                            }

                        /*如果该URL符合正则表达式且不在已访问的URL队列中,那么就将它存到待访问的队列中*/
                            if (realUrl != null && !visitedQueue.containsUrl(realUrl) && !unVisitedQueue.contains
                                    (realUrl)) {
                            /*存储到UnVisited的队列中*/
                                unVisitedQueue.addUrl(realUrl);
                                System.out.println("获取的URL:" + realUrl);
                            }

                        }

                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*通过正则表达式确定该链接是否正确*/
        public boolean regexString(String targetStr) {
            String patternStr = "/tag/.*?\\?start=\\d+&amp;type=T";

            Pattern pattern = Pattern.compile(patternStr);

            Matcher matcher = pattern.matcher(targetStr);

            return matcher.find();
        }


        /*解析网页资源,将资源写入文件并且从中找到和当前链接格式一致的URL(下一页)*/
        public String parseEntity(InputStream entityContent) {

            StringBuffer res = new StringBuffer();
            BufferedReader reader = null;
            try {
                if (entityContent == null) {
                    return "";
                }

                reader = new BufferedReader(new InputStreamReader(
                        entityContent));
                String line = null;

                while ((line = reader.readLine()) != null) {
                    res.append(line);
                }

                return res.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                }
            }
            return "";
        }
    }
}
