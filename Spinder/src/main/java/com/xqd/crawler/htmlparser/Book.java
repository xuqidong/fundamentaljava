package com.xqd.crawler.htmlparser;

/**
 * Created by xqd on 2017/4/12.
 */
public class Book {

    private String title;
    private String publish;
    private String star;
    private String description;

    public Book(String title, String publish, String star, String description) {
        this.title = title;
        this.publish = publish;
        this.star = star;
        this.description = description;
    }

    public Book() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
