package com.xqd.crawler.htmlparser;

import org.htmlparser.Node;
import org.htmlparser.NodeFilter;
import org.htmlparser.Parser;
import org.htmlparser.filters.HasAttributeFilter;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by xqd on 2017/4/14.
 */
public class ParserHtml extends Thread {

    private String parserUrl = null;

    public ParserHtml(String url) {
        parserUrl = url;
    }

    @Override
    public void run() {
        List<Book> list = new ArrayList<Book>();
        try {
//            Parser parser = new Parser((HttpURLConnection)
//                    new URL("https://book.douban.com/tag/%E5%8E%86%E5%8F%B2")
//                            .openConnection());

            /*根据string进行解析*/
            Parser parser = new Parser(parserUrl);

            /*这个是标签,所以这能过滤出html定义的标签,但是不能过滤出属性*/
//            NodeFilter tagFilter = new TagNameFilter("DIV");
            NodeFilter attributeFilter = new HasAttributeFilter("class", "info");
            NodeList nodes = parser.extractAllNodesThatMatch(attributeFilter);

            Node node = nodes.elementAt(0);
            NodeList children = node.getChildren();
            for (int i = 0; i < children.size(); i++) {
                Node childNode = children.elementAt(i);
                String text = childNode.getText();
                String plainTextString = childNode.toPlainTextString();
                System.out.println(plainTextString.trim());
                Book book = new Book();
                if (text.equals("h2 class=\"\"")) {
                    book.setTitle(plainTextString.trim());
                }
                if (text.equals("div class=\"pub\"")) {
                    book.setPublish(plainTextString.trim());
                }
                if (text.equals("div class=\"star clearfix\"")) {
                    book.setStar(plainTextString.trim());
                }
                if (text.equals("p")) {
                    book.setDescription(plainTextString.trim());
                }
                list.add(book);
            }

            Iterator<Book> iterator = list.iterator();
            while (iterator.hasNext()) {
                Book book = iterator.next();
                System.out.println(book.getTitle());
                System.out.println(book.getPublish());
                System.out.println(book.getStar());
                System.out.println(book.getDescription());
            }

        } catch (ParserException e) {
            e.printStackTrace();
        }
    }
}
