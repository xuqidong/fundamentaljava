package com.xqd.crawler.htmlparser;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by xqd on 2017/4/14.
 */
public class UnVisitedQueue {

    private volatile int count = 0;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    /*使用阻塞队列完成URL的存储*/
//    private static LinkedBlockingDeque<String> linkedBlockingDeque = new LinkedBlockingDeque<String>();
    private ConcurrentLinkedQueue concurrentLinkedQueue = new ConcurrentLinkedQueue();


    /*添加URL*/
    public void addUrl(String url) {
        count++;
//        linkedBlockingDeque.addLast(url);
        concurrentLinkedQueue.add(url);
    }

    /*移除已访问的URL*/
    public void removeUrl(String url) {
//        linkedBlockingDeque.removeFirst();
        concurrentLinkedQueue.remove(url);
    }

    /*获取第一个URL*/
    public String getUrl() {
//        return linkedBlockingDeque.pop();
        String visitingUrl = (String) concurrentLinkedQueue.peek();
        removeUrl(visitingUrl);
        return  visitingUrl;

    }

    public boolean isEmpty() {
        return concurrentLinkedQueue.isEmpty();
//        return linkedBlockingDeque.isEmpty();
    }

    public int size() {
        return concurrentLinkedQueue.size();
//        return linkedBlockingDeque.size();
    }

    public boolean contains(String url){
        return concurrentLinkedQueue.contains(url);
    }

}
