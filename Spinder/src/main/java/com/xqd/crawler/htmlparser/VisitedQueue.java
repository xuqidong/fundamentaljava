package com.xqd.crawler.htmlparser;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by xqd on 2017/4/14.
 */
public class VisitedQueue {

//    private LinkedBlockingDeque<String> sLinkedBlockingDeque = new LinkedBlockingDeque<>();
    private ConcurrentLinkedQueue concurrentLinkedQueue = new ConcurrentLinkedQueue();

    public void addVisitedUrl(String url) {
     concurrentLinkedQueue.add(url);
//        sLinkedBlockingDeque.add(url);
    }

    public boolean containsUrl(String url) {
        return concurrentLinkedQueue.contains(url);
//        return sLinkedBlockingDeque.contains(url);
    }

    public String getUrl() {
        return (String) concurrentLinkedQueue.poll();
//        return sLinkedBlockingDeque.element();
    }

    public int size() {
        return concurrentLinkedQueue.size();
//        return sLinkedBlockingDeque.size();
    }

    public int getCount() {
        return concurrentLinkedQueue.size();
//        return sLinkedBlockingDeque.size();
    }
}